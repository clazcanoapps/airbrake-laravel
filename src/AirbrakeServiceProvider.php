<?php namespace rigor789\AirbrakeLaravel;

use Illuminate\Support;
use Illuminate\Support\ServiceProvider;
use Exception;
use Airbrake;

class AirbrakeServiceProvider extends ServiceProvider {

    /**
     * Load the provider only if it's required
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Catch all exceptions and send them to Airbrake if enabled.
     *
     * @return void
     */
    public function boot()
    {
        $this->package('rigor789/airbrake-laravel', 'airbrake-laravel', realpath(__DIR__));

        if ( ! $this->isEnabled())
        {
            return;
        }

        $app = $this->app;

        $app->error(
            function (Exception $exception) use ($app)
            {
                $app['airbrake']->notify($exception);
            }
        );

        $app->fatal(
            function ($exception) use ($app)
            {
                $app['airbrake']->notify($exception);
            }
        );
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            'airbrake',
            function ($app)
            {
                $options = [
                    'projectId' => $app['config']->get('airbrake.project_id'), // FIX ME
                    'projectKey'      => $app['config']->get('airbrake.api_key'),
                    'environment' => $app->environment(),
                ];

                $notifier = new Airbrake\Notifier($options);

                Airbrake\Instance::set($notifier);

                $handler = new Airbrake\ErrorHandler($notifier);
                $handler->register();

                return  $notifier;
            }
        );
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['airbrake'];
    }

    /**
     * Should we send exceptions to Airbrake?
     *
     * @return bool
     */
    protected function isEnabled()
    {
        $enabled = $this->app['config']->get('airbrake.enabled', false);
        $ignored = $this->app['config']->get('airbrake.ignore_environments', []);

        return $enabled && ! in_array($this->app->environment(), $ignored);
    }
}
